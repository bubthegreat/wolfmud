FROM golang:1.17-alpine

# Copy and build the server binary.
WORKDIR /wolfmud
COPY . .
RUN go build -o bin ./...

# Expose the default port.
EXPOSE 4001

# Serving directory - this is required because
# wolfmud assumes it is being run from /wolfmud/bin
WORKDIR /wolfmud/bin

# Default command an be overridden to run /wolfmud/bin/botrunner if required.
CMD /wolfmud/bin/server
